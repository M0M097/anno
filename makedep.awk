#!/usr/bin/awk -f
#
# Generates a Makefile containing dependencies of a fortran project and
# defining the program routine
# Author: Moritz R. Schaefer
#

# Strip file extension since we mainly need the name of the object files
FNR == 1 { sub(".f90","",FILENAME) }

# Find program routine and its defining file
tolower($1) == "program" {
    print "MAIN = " BLDDIR FILENAME ".o"
    $2 = $2 ? $2 : FILENAME # If program name is undefined use the filename
    print "PROGRAM = " $2 EXT
    print "ARCHIVE = " BLDDIR $2 AREXT
}

# Check if file depends on preprocessor flags
$1 == "#ifdef" { print BLDDIR FILENAME".o: " PREPMEM }

# Index files with the modules they define.
tolower($1) == "module" { DEF[tolower($2)] = FILENAME }

# Index module with files which use them
tolower($1) == "use" { USE[FILENAME] = tolower($2) ","  USE[FILENAME] }

# Index module with files containing its submodules
tolower($1) == "submodule" {  
    sub(/^.*\([ \t]*/,"") # Strip everyting till the name of the module
    sub(/[ \t]*\).*$/,"") # Strip everyting after the name of the module
    USE[FILENAME] = tolower($0) ","  USE[FILENAME] 
}

END {
    # Write the dependies of each file that uses (comma seperated) modules,...
    for ( FILE in USE ) for ( i = split ( USE[FILE], MODS, "," ); i--; )
        ## ...once, if they are defined in another file.
        if ( DEF[MODS[i]] && !a[FILE,MODS[i]]++ && DEF[MODS[i]] != FILE )
            print BLDDIR FILE".o: " BLDDIR DEF[MODS[i]]".o"
}
