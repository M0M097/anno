! Implements a the WELL512a random number generator
module mod_well512a

    use iso_fortran_env, only: DP => REAL64

    implicit none

    private
    public :: random_uniform, shuffle

    integer, parameter :: W = 32
    integer, parameter :: R = 16
    integer, parameter :: MASK1 = int(z'00000001')
    integer, parameter :: MASK2 = int(z'0000000f')
    integer, parameter :: MASK3 = int(z'000000ff')
    integer, parameter :: MASK4 = int(z'0000ffff')
    integer, parameter :: MASK5 = int(z'7fffffff')
    integer, parameter :: MAT1 = 13
    integer, parameter :: MAT2 = 9
    integer, parameter :: MAT3 = 5

    integer, dimension(0:W-1) :: state
    integer :: index
    logical :: initialized = .false.

    interface initialize_rng
        procedure :: initialize_from_seed
        procedure :: initialize_from_time
    end interface initialize_rng

contains

    impure subroutine initialize_from_time()
        integer :: seed_generator(8)
        call date_and_time(values = seed_generator)
        call initialize_from_seed(product(seed_generator(5:8) + 1))
        ! current hour * minutes * seconds * miliseconds; fits in 4byte integer
        ! + 1 is necessary to avoid zero and that rolls can be predicted if they
        ! start at a "full" hour/minute.
    end subroutine initialize_from_time

    subroutine initialize_from_seed(seed)
        integer, intent(in) :: seed
        integer :: i, idummy

        initialized = .true.

        ! Initialize the state array
        state = 0
        state(0) = seed

        do i = 1, W - 1
            state(i) = ieor(1812433253 * ieor(state(i - 1), ishft(state(i - 1), -30)), i)
        end do

        index = 0

        ! Discard the first 10000 values
        do i = 1, 10000
            idummy = random_uniform()
        end do
    end subroutine initialize_from_seed

    real(DP) function random_uniform()
        integer :: z0, z1, z2
        if (.not. initialized) call initialize_rng

        z0 = state(index)
        z1 = ieor(state(mod(index + MAT1, W)), iand(state(mod(index + MAT2, W)), MASK1))
        z2 = ieor(state(mod(index + MAT3, W)), iand(state(mod(index + 0, W)), MASK2))

        state(index) = ieor(z1, ieor(z0, ishft(z2, -R)))

        index = mod(index + 1, W)

        ! Convert to a double precision value between 0 and 1
        random_uniform = iand(state(index), MASK5) / 2.147483647e9_DP
    end function random_uniform

    ! shuffles a list of integers in place
    ! Uses the Knuth shuffle algorithm
    subroutine shuffle(n, a)
        integer, intent(in) :: n !< size of array
        integer, intent(inout) :: a(*) !< array to be shuffled
        integer :: i, ranDPos, temp
        real(DP) :: r

        do i = n, 2, -1
            ranDPos = ceiling(random_uniform() * i)
            temp = a(ranDPos)
            a(ranDPos) = a(i)
            a(i) = temp
        end do

    end subroutine shuffle

end module mod_well512a
