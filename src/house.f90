module mod_house

    use mod_const, only: &
        NGOODS => NUM_GOODS, &
        COSTS  => HOUSE_COSTS, &
        LBUYS  => HOUSE_LBUYS, &
        PAYS   => HOUSE_PAYS, &
        NAMES  => HOUSE_NAMES, &
        NLEVEL => N_HOUSE_LEVELS, &
        NPUB_BUILDINGS_PER_PLAYER
    use mod_good, only: Good
    use mod_money, only: Money
    use mod_public_buildings, only: PublicBuilding
    use mod_encoding, only: CK, UE
    use mod_fail, only: fail

    implicit none
    private

    integer, parameter :: UNBEBAUT = 1
    integer, parameter :: PIONIER  = 2
    integer, parameter :: SIEDLER  = 3
    integer, parameter :: BUERGER  = 4
    integer, parameter :: KAUFMANN = 5

    public :: House

    type :: House
        private
        integer :: level = 1
        logical :: lhas_bought = .false.
    contains
        private
        procedure, pass :: lis
        procedure, pass, public :: lis_kaufmann
        procedure, pass, public :: lis_buerger
        procedure, pass, public :: lis_siedler
        procedure, pass, public :: lis_pionier
        procedure, pass, public :: upgrade
        procedure, pass, public :: upgrade_for_free
        procedure, pass, public :: get_costs
        procedure, pass, public :: get_pay
        procedure, pass, public :: get_name
        procedure, pass, public :: ldoes_buy
        procedure, pass, public :: sell
        procedure, pass, public :: reset
        procedure, pass, public :: lisbuild
        procedure, pass, public :: burns_down
        procedure, pass, public :: get_level
    end type

    interface House
        module procedure constructor
    end interface House

contains

    logical elemental function lis_kaufmann(self)
        class(House), intent(in) :: self
        lis_kaufmann = self%lis(KAUFMANN)
    end function lis_kaufmann

    logical elemental function lis_buerger(self)
        class(House), intent(in) :: self
        lis_buerger = self%lis(BUERGER)
    end function lis_buerger

    logical elemental function lis_siedler(self)
        class(House), intent(in) :: self
        lis_siedler = self%lis(SIEDLER)
    end function lis_siedler

    logical elemental function lis_pionier(self)
        class(House), intent(in) :: self
        lis_pionier = self%lis(PIONIER)
    end function lis_pionier

    logical elemental function lis(self, itype)
        class(House), intent(in) :: self
        integer, intent(in) :: itype
        lis = self%level == itype
    end function lis

    elemental type(House) function constructor(level) result(self)
        integer, intent(in) :: level
        self%level = level
    end function

    pure subroutine upgrade(self, tools, err, msg)
        class(House), intent(in out) :: self
        type(Good), intent(in out) :: tools(NGOODS)
        logical, intent(out), optional  :: err
        character(kind = CK, len = 200), intent(out), optional :: msg

        if (any(tools%get() - self%get_costs() < 0)) then
            err = .true.
            msg = CK_"Nicht genug Rohstoffe"
            return
        end if

        call tools%set(tools%get() - self%get_costs())
        call self%upgrade_for_free(err, msg)
        if (self%level == 2) msg = CK_"Pionier gebaut"
    end subroutine upgrade

    pure subroutine upgrade_for_free(self, err, msg)
        class(House), intent(in out) :: self
        logical, intent(out) :: err
        character(kind = CK, len = 200), intent(out) :: msg

        if (self%level == NLEVEL) then
            err = .true.
            msg = CK_"Bereits maximales Level erreicht"
        else
            err = .false.
            self%lhas_bought = .false.
            msg = trim(adjustl(NAMES(self%level))) // CK_" zu " // &
                trim(adjustl(NAMES(self%level + 1))) // CK_" aufgewertet."
            self%level = self%level + 1
        end if
    end subroutine upgrade_for_free

    pure function get_costs(self)
        class(House), intent(in) :: self
        integer :: get_costs(NGOODS)
        get_costs = COSTS(:, self%level)
    end function get_costs

    elemental integer function get_pay(self)
        class(House), intent(in) :: self
        get_pay = PAYS(self%level)
    end function get_pay

    elemental character(kind = CK, len = 9) function get_name(self)
        class(House), intent(in) :: self
        get_name = NAMES(self%level)
    end function get_name

    elemental logical function ldoes_buy(self, merchandise)
        class(House), intent(in) :: self
        type(Good), intent(in) :: merchandise
        ldoes_buy = LBUYS(merchandise%get_idx(), self%level) .and. .not. self%lhas_bought
    end function ldoes_buy

    pure subroutine sell(self, merchandise, gold, public_buildings, lerr, msg)
        class(House), intent(in out) :: self
        type(Good), intent(in out) :: merchandise
        type(Money), intent(in out) :: gold
        type(PublicBuilding), intent(in) :: public_buildings(NPUB_BUILDINGS_PER_PLAYER)
        logical, intent(out) :: lerr
        character(kind = CK, len = 200), intent(out) :: msg
        integer :: selling_bonus
        integer :: effective_pay

        ! Check if house does buy good
        if (self%lhas_bought .or. .not. merchandise%lisenough(1)) then
            call fail(__LINE__, __FILE__, CK_"house%sell called incorrectly")
        end if

        ! Check for selling boni from public buildings
        if ( &
            (self%level == PIONIER .and. any(public_buildings%lis_schule())) &
            .or. &
            (self%level == BUERGER .and. any(public_buildings%lis_schenke())) &
        ) then

            selling_bonus = 1

        else if ( &
            (self%level == KAUFMANN .and. any(public_buildings%lis_badehaus())) &
            .or. &
            (self%level == SIEDLER  .and. any(public_buildings%lis_schenke())) &
        ) then

            selling_bonus = 2

        else

            selling_bonus = 0

        end if

        ! Sell
        effective_pay = self%get_pay() + selling_bonus
        call merchandise%decrement()
        call gold%add(effective_pay)
        self%lhas_bought = .true.
        lerr = .false.
        write(msg, '(A, I1, A)') merchandise%get_name() // CK_" f" // UE // CK_"r ", effective_pay, CK_" verkauft"

    end subroutine sell

    elemental subroutine reset(self)
        class(House), intent(in out) :: self
        self%lhas_bought = .false.
    end subroutine reset

    elemental logical function lisbuild(self)
        class(House), intent(in) :: self
        lisbuild = self%level > UNBEBAUT
    end function

    elemental integer function get_level(self)
        class(House), intent(in) :: self
        get_level = self%level
    end function get_level

    elemental subroutine burns_down(self)
        class(House), intent(in out) :: self
        call self%reset
        self%level = 1
    end subroutine burns_down

end module mod_house
