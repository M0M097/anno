module mod_const

    use mod_encoding, only: CK, UE, BLU, RED, YEL, GRN, CLEN, VBAR, U253C

    implicit none

    integer, private :: i

    integer, parameter :: N_HOUSE_LEVELS       =  5
    integer, parameter :: NUM_GOODS            =  6
    integer, parameter :: INITIAL_BUYING_PRICE =  6
    integer, parameter :: MAX_NUM_HOUSES       =  7
    integer, parameter :: MAX_MONEY            = 40
    integer, parameter :: MAX_PLAYER           =  4
    integer, parameter :: MAX_HARBOUR          =  5
    integer, parameter :: MAX_TRADE_PARTNERS   =  3

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Ships
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, parameter :: SHIP_COSTS(NUM_GOODS) = [ &
        ! Holz, Stein, Werkzeug, Tuch, Gewürz, Taback
        &  1,     0,      1,       1,    0,       0 &
    ]
    character(kind = CK, len = *), parameter :: SAIL_MENU = &
        & CK_"1) " // CK_"Nach oben"   // new_line(CK_'A') // &
        & CK_"2) " // CK_"Nach rechts" // new_line(CK_'A') // &
        & CK_"3) " // CK_"Nach unten"  // new_line(CK_'A') // &
        & CK_"4) " // CK_"Nach links"  // new_line(CK_'A')
    integer, parameter :: UP = 1, RIGHT = 2, DOWN = 3, LEFT = 4

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! HOUSES
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, parameter :: HOUSE_COSTS(NUM_GOODS, N_HOUSE_LEVELS) = reshape([ &
        ! Holz, Stein, Werkzeug, Tuch, Gewürz, Taback
        &  1,     0,      1,       0,    0,       0, & ! Pionier
        &  0,     2,      0,       1,    0,       0, & ! Siedler
        &  0,     1,      0,       0,    2,       0, & ! Bürger
        &  0,     0,      0,       0,    1,       2, & ! Kaufmann
        &  9,     9,      9,       9,    9,       9  & ! Can not be build
    ], [NUM_GOODS, N_HOUSE_LEVELS])

    character(kind = CK, len = 9), parameter :: HOUSE_NAMES(N_HOUSE_LEVELS) = [ &
        & CK_"   -     ", &
        & CK_"Pionier  ", &
        & CK_"Siedler  ", &
        & CK_"B" // UE // CK_"rger   " , &
        & CK_"Kaufmann " &
    ]

    logical, parameter :: HOUSE_LBUYS(NUM_GOODS, N_HOUSE_LEVELS) = reshape([ &
        ! !Holz, Stein, Werkzeug, Tuch,   Gewürz,   Taback
        & .false., .false., .false., .false., .false., .false., & ! Nothing
        & .true. , .true. , .true. , .false., .false., .false., & ! Pionier
        & .false., .false., .false., .true. , .false., .false., & ! Siedler
        & .false., .false., .false., .false., .true. , .false., & ! Bürger
        & .false., .false., .false., .false., .false., .true.   & ! Kaufmann
    ], [NUM_GOODS, N_HOUSE_LEVELS])

    integer, parameter :: HOUSE_PAYS(N_HOUSE_LEVELS) = [(i - 1, i = 1, N_HOUSE_LEVELS)]

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Goods
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, parameter :: IHOLZ     = 1
    integer, parameter :: ISTEIN    = 2
    integer, parameter :: IWERKZEUG = 3
    integer, parameter :: ITUCH     = 4
    integer, parameter :: IGEWUERZ  = 5
    integer, parameter :: ITABBACK  = 6
    integer, parameter :: NINVENTORY_LINES = NUM_GOODS / 2 + 1

    character(kind = CK, len = 8), parameter :: GOOD_NAMES(0:NUM_GOODS) = [ &
        & CK_"        ", &
        & CK_"    Holz", &
        & CK_"   Stein", &
        & CK_"Werkzeug", &
        & CK_"    Tuch", &
        & CK_" Gew" // UE // CK_"rz ",  &
        & CK_"  Taback"  &
    ]

    character(kind = CK), private, parameter :: SEPERATOR = &
        new_line(CK_'A')
        ! ' '
    character(kind = CK, len = *), parameter :: ACTION_MENU = &
        & CK_"1: Einen Rohstoff kaufen"     // SEPERATOR // &
        & CK_"2: Haus bauen/aufwerten"      // SEPERATOR // &
        & CK_"3: Rohstoff verkaufen"        // SEPERATOR // &
        & CK_"4: Schiff bauen"              // SEPERATOR // &
        & CK_"5: Informationen anzeigen"    // SEPERATOR // &
        & CK_"6: Schiff fahren/Zug beenden" // SEPERATOR

    integer, parameter :: GOOD_INI_STOCK(NUM_GOODS) = [ 1, 1, 0, 0, 0, 0 ]
    integer, parameter :: GOOD_INDEX(NUM_GOODS) = [(i, i = 1, NUM_GOODS)]
    integer, parameter :: HOUSE_INI_LEVEL(MAX_NUM_HOUSES) = [ 3, 2, (1, i = 3, MAX_NUM_HOUSES)]

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Map
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, parameter :: BARR = -1
    integer, parameter :: SEA  = 0
    integer, parameter :: WOOD = 1
    integer, parameter :: STON = 2
    integer, parameter :: TOOL = 3
    integer, parameter :: WOOL = 4
    integer, parameter :: SPIC = 5
    integer, parameter :: TBCO = 6
    integer, parameter :: INDI = 7 ! Indian island
    integer, parameter :: TRS1 = 8 ! Treasure to upgrade house
    integer, parameter :: TRS2 = 9 ! Treasure 12 gold

    integer, parameter :: NISLANDS_PER_PLAYER = 5
    integer, parameter :: NUM_ISLANDS = 9
    character(kind = CK, len = *), parameter :: SPECIAL_ISLANDS(NUM_GOODS + 1:NUM_ISLANDS) = [ &
        CK_"ein Handelspartner                          ", &
        CK_"ein Kulturschatz (Einwohner entwickelt sich)", &
        CK_"ein Goldschatz (+12 Gold)                   " &
    ]
    character(kind = CK), parameter :: NAME_MAP(-1:NUM_ISLANDS) = [ &
        'x', '~', [('I', i = 1, NUM_ISLANDS)] &
    ]

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Public buildings
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, parameter :: NPUBLIC_BUILDINGS = 8
    integer, parameter :: NPUBLIC_BUILDINGS_NLINES = NPUBLIC_BUILDINGS / 2 + 3
    integer, parameter :: NPUB_BUILDINGS_PER_PLAYER = 4

    integer, parameter :: INITIAL_PUBLIC_BUILDINGS(NPUBLIC_BUILDINGS, MAX_PLAYER) = reshape([ &
    ! SCHMIEDE FEUERWEHR KIRCHE SCHULE SCHENKE BADEHAUS WERFT KONTOR
        1,         1,       1,     1,     1,      1,      1,     1, & ! One player, just for testing
        1,         1,       1,     1,     1,      1,      1,     0, & ! Two players
        1,         1,       1,     2,     1,      1,      2,     1, & ! Three players
        2,         2,       1,     2,     2,      1,      2,     2  & ! Four players TODO
    ], [NPUBLIC_BUILDINGS, MAX_PLAYER])

    integer, parameter :: PB_OFFSET = MAX_NUM_HOUSES - NPUB_BUILDINGS_PER_PLAYER

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Formatting
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, parameter, public :: NFIELD = 25
    character(kind = CK, len = NFIELD + 1), parameter :: DELIM = repeat(VBAR, NFIELD) // U253C
    character(kind = CK, len = *), parameter :: FFMT = '(A25)'
    integer, parameter, public :: INFO_STR_LEN = &
        2 + & ! name + delimiter
        NINVENTORY_LINES + 1 + & ! (goods / 2) + gold + delimiter
        MAX_NUM_HOUSES + 1 + & ! houses + delimiter
        2 + & ! points + delimiter
        2 + & ! trade_partners + delimiter
        NISLANDS_PER_PLAYER + 1 ! islands + delimiter

    character(kind = CK, len = CLEN), parameter :: COLORS(MAX_PLAYER) = [BLU, RED, YEL, GRN]

end module mod_const
