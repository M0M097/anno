module mod_info

    use mod_player, only: Player
    use mod_const, only: INFO_STR_LEN, NFIELD, MAX_PLAYER, NPUBLIC_BUILDINGS_NLINES
    use mod_encoding, only: CK, DEFAULT
    use mod_public_buildings, only: remaining_public_buildings_str
    use mod_map, only: NLINES, world
    ! use mod_io, only: center

    implicit none

    private

    integer, parameter :: NLIN = NFIELD * (MAX_PLAYER + 1) + 5

    public :: send_global_info

contains

    impure subroutine send_global_info(nplayers, players, receiver)
        integer, intent(in) :: nplayers
        type(Player), intent(in) :: players(nplayers)
        type(Player), intent(in), optional :: receiver
        character(kind = CK, len = NLIN) :: line
        character(kind = CK, len = NFIELD + 1) :: info_str(INFO_STR_LEN, nplayers + 1)
        character(kind = DEFAULT, len=6) :: ffmt
        character(kind = CK), allocatable :: cship_names(:)
        integer, allocatable :: ship_positions(:, :)
        integer :: i, j

        ! Concat stiring
        do i = 1, nplayers
            info_str(:, i) = players(i)%info_str()
        end do
        info_str(1:NPUBLIC_BUILDINGs_NLINES, nplayers + 1) = remaining_public_buildings_str()
        info_str(NPUBLIC_BUILDINGs_NLINES + 1:, nplayers + 1) = CK_" "

        ! Map
        allocate(ship_positions(2, nplayers * 2), cship_names(nplayers * 2))
        do i = 1, nplayers
            ship_positions(:, i    ) = players(i)%ships(1)%get_position()
            ship_positions(:, i + nplayers) = players(i)%ships(2)%get_position()
            cship_names(i    ) = players(i)%get_first_letter()
            cship_names(i + nplayers) = players(i)%get_first_letter()
        end do
        info_str(INFO_STR_LEN - NLINES + 1:, nplayers + 1) = world%csnapshot_str(ship_positions, cship_names)

        ! Reset terminal
        if (present(receiver)) then
            call receiver%send_msg(char(27, kind = CK) // CK_"c")
        else
            call players%send_msg(char(27, kind = CK) // CK_"c")
        end if

        ! Send info
        write(ffmt, '(A1, I1, A1, I2, A1)') '(', nplayers + 1, 'A', NFIELD + 1, ')'
        do i = 1, INFO_STR_LEN
            write(line, ffmt) (info_str(i, j), j = 1, nplayers + 1)
            if (present(receiver)) then
                call receiver%send_msg(line)
            else
                call players%send_msg(line)
            end if
            ! print *, line
        end do
    end subroutine send_global_info

end module mod_info
