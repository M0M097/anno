module mod_public_buildings

    use mod_const, only: &
        NPUBLIC_BUILDINGS, INITIAL_PUBLIC_BUILDINGS, &
        NPUBLIC_BUILDINGS_NLINES, &
        NFIELD, DELIM
    use mod_io, only: STDOUT, idecision, center
    use mod_fail, only: fail
    use mod_encoding, only: CK, OE, AE

    implicit none

    private

    integer :: bank_public_buildings(NPUBLIC_BUILDINGS)
    logical :: initialized = .false.

    integer, parameter :: NLEN_NAMES = 9
    character(kind = CK, len= NLEN_NAMES), parameter :: NAME_PUBLIC_BUILDINGS(0:NPUBLIC_BUILDINGS) = [ &
        CK_"    -    ", &
        CK_"Schmiede ", &
        CK_"Feuerwehr", &
        CK_"Kirche   ", &
        CK_"Schule   ", &
        CK_"Schenke  ", &
        CK_"Badehaus ", &
        CK_"Werft    ", &
        CK_"Kontor   "  &
    ]

    integer, parameter :: NOT_BUILD  = 0
    integer, parameter :: ISCHMIEDE  = 1
    integer, parameter :: IFEUERWEHR = 2
    integer, parameter :: IKIRCHE    = 3
    integer, parameter :: ISCHULE    = 4
    integer, parameter :: ISCHENKE   = 5
    integer, parameter :: IBADEHAUS  = 6
    integer, parameter :: IWERFT     = 7
    integer, parameter :: IKONTOR    = 8

    public :: PublicBuilding

    type :: PublicBuilding
        private
        integer :: type = NOT_BUILD
    contains
        private
        procedure, pass :: lis

        procedure, pass, public :: get_name
        procedure, pass, public :: lis_schmiede
        procedure, pass, public :: lis_feuerwehr
        procedure, pass, public :: lis_kirche
        procedure, pass, public :: lis_schule
        procedure, pass, public :: lis_schenke
        procedure, pass, public :: lis_badehaus
        procedure, pass, public :: lis_werft
        procedure, pass, public :: lis_Kontor
        procedure, pass, public :: lis_build
        procedure, pass, public :: destroy
    end type

    public :: set_bank_public_buildings
    public :: choose_public_building
    public :: remaining_public_buildings_str

contains

    logical elemental function lis_schmiede(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(ISCHMIEDE)
    end function lis_schmiede

    logical elemental function lis_feuerwehr(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(IFEUERWEHR)
    end function lis_feuerwehr

    logical elemental function lis_kirche(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(IKIRCHE)
    end function lis_kirche

    logical elemental function lis_schule(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(ISCHULE)
    end function lis_schule

    logical elemental function lis_schenke(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(ISCHENKE)
    end function lis_schenke

    logical elemental function lis_badehaus(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(IBADEHAUS)
    end function lis_badehaus

    logical elemental function lis_werft(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(IWERFT)
    end function lis_werft

    logical elemental function lis_kontor(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%lis(IKONTOR)
    end function lis_kontor

    logical elemental function lis_build(self) result(lis)
        class(PublicBuilding), intent(in) :: self
        lis = self%type /= NOT_BUILD
    end function lis_build

    logical elemental function lis(self, itype)
        class(PublicBuilding), intent(in) :: self
        integer, intent(in) :: itype
        lis = self%type == itype
    end function lis

    pure function get_name(self)
        class(PublicBuilding), intent(in) :: self
        character(kind = CK, len = :), allocatable :: get_name
        get_name = trim(adjustl(NAME_PUBLIC_BUILDINGS(self%type)))
    end function get_name

    impure subroutine set_bank_public_buildings(nplayers)
        integer, intent(in) :: nplayers
        bank_public_buildings = initial_public_buildings(:, nplayers)
        initialized = .true.
    end subroutine set_bank_public_buildings

    logical function lremaining_public_buildings()
        lremaining_public_buildings = any(bank_public_buildings > 0)
    end function lremaining_public_buildings

    impure elemental subroutine destroy(self)
        class(PublicBuilding), intent(in out) :: self
        call increment_bank_public_building(self%type)
        self%type = NOT_BUILD
    end subroutine destroy

    impure subroutine choose_public_building(player_name, lerr, msg, public_building)
        character(kind = CK, len = *), intent(in) :: player_name
        logical, intent(out) :: lerr
        character(kind = CK, len = 200), intent(out) :: msg
        type(PublicBuilding), intent(out) :: public_building
        integer :: i, n, mapping(NPUBLIC_BUILDINGS)
        integer :: itype

        if (.not. initialized) call fail(__LINE__, __FILE__, CK_"Public Buildings were not initialized")

        n = 0
        lerr = .false.

        if (.not. lremaining_public_buildings()) then
            msg = CK_"Keine verbleibenden " // OE // CK_"ffentlichen Geb" // AE// CK_"ude"
            public_building = PublicBuilding()
            lerr = .true.
            return
        end if

        write(STDOUT, '(A)') CK_"Du kannst zwischen den folgenden, &
            &verbleibenden" // OE // CK_"ffentlichen Geb" // AE // CK_"uden w" // AE // CK_"hlen: "
        do i = 1, NPUBLIC_BUILDINGS
            if (bank_public_buildings(i) > 0) then
                n = n + 1
                mapping(n) = i
                write(STDOUT, '(I1, A)') n, CK_") " // NAME_PUBLIC_BUILDINGS(i)
            end if
        end do
        itype = mapping(idecision(n))
        public_building = PublicBuilding(type = itype)
        call decrement_bank_public_building(itype)
        msg = player_name // CK_" hat das " // OE // CK_"ffentliche Geb" // AE // CK_"ude " // &
            public_building%get_name() // CK_" errichtet"
    end subroutine choose_public_building

    impure elemental subroutine decrement_bank_public_building(ibuilding)
        integer, intent(in) :: ibuilding
        if (ibuilding < 1 .or. ibuilding > NPUBLIC_BUILDINGS .or. bank_public_buildings(ibuilding) == 0) &
            call fail(__LINE__, __FILE__)
        bank_public_buildings(ibuilding) = bank_public_buildings(ibuilding) - 1
    end subroutine decrement_bank_public_building

    impure elemental subroutine increment_bank_public_building(ibuilding)
        integer, intent(in) :: ibuilding
        if (ibuilding < 1 .or. ibuilding > NPUBLIC_BUILDINGS) call fail(__LINE__, __FILE__)
        bank_public_buildings(ibuilding) = bank_public_buildings(ibuilding) + 1
    end subroutine increment_bank_public_building

    pure function remaining_public_buildings_str() result(str)
        integer :: i
        character(kind = CK, len = NFIELD) :: str(NPUBLIC_BUILDINGS_NLINES)

        str(1) = center(OE // CK_"ffentliche Geb" // AE // CK_"ude")
        str(2) = DELIM

        do i = 1, NPUBLIC_BUILDINGS / 2
            write(str(2 + i), '(I1, 1x, A, 2x, I1, 1x, A)') &
                bank_public_buildings(i), NAME_PUBLIC_BUILDINGS(i), &
                bank_public_buildings(i + (NPUBLIC_BUILDINGS / 2)), NAME_PUBLIC_BUILDINGS(i + (NPUBLIC_BUILDINGS / 2))
        end do

        str(NPUBLIC_BUILDINGS_NLINES) = DELIM
    end function remaining_public_buildings_str

end module mod_public_buildings
