module mod_ship
    use mod_const, only: NUM_GOODS, SHIP_COSTS
    use mod_map, only: world
    use mod_good, only: Good
    use mod_const, only: GOOD_NAMES, SPECIAL_ISLANDS, UP, DOWN, LEFT, RIGHT
    use mod_fail, only: fail
    use mod_encoding, only: CK, OE

    implicit none
    private

    public :: Ship

    type :: Ship
        private
        integer :: h_pos = 1
        integer :: w_pos = 1
        integer :: costs(NUM_GOODS) = SHIP_COSTS
        logical :: lbuild
    contains
        private
        procedure, pass, public :: sail
        procedure, pass, public :: build
        procedure, pass, public :: lis_build
        procedure, pass, public :: destroy
        procedure, pass, public :: get_position
    end type Ship

    interface Ship
        procedure constructor
    end interface Ship

contains

    type(Ship) elemental function constructor(lbuild) result(self)
        logical, intent(in) :: lbuild
        self%lbuild = lbuild
    end function constructor

    pure subroutine build(self, tools, lerr, msg)
        class(Ship), intent(in out) :: self
        type(Good), intent(in out) :: tools(NUM_GOODS)
        logical, intent(out) :: lerr
        character(kind = CK, len = 200), intent(out) :: msg

        if (any(tools%get() - self%costs < 0)) then
            msg = CK_"Nicht genug Rohstoffe"
            lerr = .true.
        else
            call tools%set(tools%get() - self%costs)
            self%lbuild = .true.
            lerr = .false.
            msg = CK_"Schiff gebaut!"
        end if
    end subroutine build

    elemental subroutine destroy(self)
        class(Ship), intent(in out) :: self
        self%lbuild = .false.
        self%h_pos = 1
        self%w_pos = 1
    end subroutine

    elemental logical function lis_build(self)
        class(Ship), intent(in) :: self
        lis_build = self%lbuild
    end function lis_build

    elemental subroutine sail(self, &
        direction, &
        ldid_sail, &
        iisland_type, msg, &
        new_wpos, new_hpos &
    )
        class(Ship), intent(in out) :: self
        integer, intent(in) :: direction
        integer, intent(out) :: new_hpos, new_wpos
        character(kind = CK, len = 200), intent(out) :: msg
        logical, intent(out) :: ldid_sail
        integer, intent(out) :: iisland_type
        character(kind = CK, len = 200) :: island_name
        ldid_sail = .false.
        select case (direction)
        case (UP)
            new_hpos = self%h_pos - 1
            new_wpos = self%w_pos
        case (RIGHT)
            new_hpos = self%h_pos
            new_wpos = self%w_pos + 1
        case (LEFT)
            new_hpos = self%h_pos
            new_wpos = self%w_pos - 1
        case (DOWN)
            new_hpos = self%h_pos + 1
            new_wpos = self%w_pos
        case default
            call fail(__LINE__, __FILE__, CK_"Unhandled error in `sail`, this should never happen")
        end select

        if (world%lis_sea(new_wpos, new_hpos)) then
            self%h_pos = new_hpos
            self%w_pos = new_wpos
            ldid_sail = .true.
            iisland_type = world%iisland_type(new_wpos, new_hpos)
            write(msg, '(A)') CK_"Schiff ist zur neuen Position gesegelt!"
        else if (world%lis_island(new_wpos, new_hpos)) then
            ldid_sail = .true.
            iisland_type = world%iisland_type(new_wpos, new_hpos)
            if (iisland_type <= NUM_GOODS) then
                write(island_name, '(A)') GOOD_NAMES(iisland_type) // CK_" Produktion"
            else
                write(island_name, '(A)') SPECIAL_ISLANDS(iisland_type)
            end if
            write(msg, '(A, I1, A)') &
                CK_"Es wurde " // trim(adjustl(island_name)) // CK_" auf der Insel entdeckt." // new_line(CK_'A') // &
                CK_"M" // OE // CK_"chtest du sie nehmen?" // new_line(CK_'A') // &
                &CK_"1) Ja" // new_line(CK_'A') //&
                &CK_"2) Nein"
        else if (world%lis_barr(new_wpos, new_hpos)) then
            write(msg, '(A)') CK_"Insel wurde bereits entdeckt!"
        else
            write(msg, '(A)') CK_"Die Inselwelt kann nicht verlassen werden!"
        end if

    end subroutine sail

    pure function get_position(self)
        class(Ship), intent(in) :: self
        integer :: get_position(2)
        if (self%lbuild) then
            get_position(1) = self%w_pos
            get_position(2) = self%h_pos
        else
            get_position = 0
        end if
    end function get_position

end module mod_ship
