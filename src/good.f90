module mod_good

    use mod_io, only: STDOUT
    use mod_encoding, only: CK

    implicit none
    private
    public :: Good

    type :: Good
        private
        character(kind = CK, len = 8) :: name ! Name of the good
        integer :: stock = 0 ! Stores how much of each item is stored
        integer :: idx = 0 ! Index of this good in all related arrays
    contains
        private
        procedure, pass :: stock_str
        procedure, pass :: idx_str
        procedure, pass, public :: print
        procedure, pass, public :: get
        procedure, pass, public :: set
        procedure, pass, public :: get_idx
        procedure, pass, public :: increment
        procedure, pass, public :: decrement
        procedure, pass, public :: lisenough
        procedure, pass, public :: get_name
        procedure, pass, public :: str
        procedure, pass, public :: menu_str
    end type

    interface Good
        module procedure constructor
    end interface Good

    contains

        type(Good) elemental function constructor(name, idx, initial_stock) result(self)
            character(kind = CK, len = *), intent(in) :: name
            integer, intent(in) :: idx
            integer, intent(in), optional :: initial_stock
            self%name = adjustl(trim(name))
            self%idx  = idx
            if (present(initial_stock)) self%stock = initial_stock
        end function constructor

        pure function get_name(self)
            class(Good), intent(in) :: self
            character(kind = CK, len = :), allocatable :: get_name
            get_name = trim(adjustl(self%name))
        end function get_name

        impure subroutine print(self)
            class(Good), intent(in) :: self
            write(STDOUT, '(A)') self%str()
        end subroutine print

        character(kind = CK) elemental function idx_str(self)
            class(Good), intent(in) :: self
            write(idx_str, '(I1)') self%idx
        end function idx_str

        character(kind = CK, len = 11) elemental function str(self)
            class(Good), intent(in) :: self
            write(str, '(I2, 1x, A8)') self%stock, self%name
        end function

        pure function menu_str(self)
            class(Good), intent(in) :: self
            character(kind = CK, len = :), allocatable :: menu_str
            menu_str = self%idx_str() // CK_": " // self%get_name() // CK_"(" // self%stock_str() // CK_")"
        end function

        character(kind = CK) elemental function stock_str(self)
            class(Good), intent(in) :: self
            write(stock_str, '(I1)') self%stock
        end function

        elemental integer function get(self)
            class(Good), intent(in) :: self
            get = self%stock
        end function get

        elemental integer function get_idx(self)
            class(Good), intent(in) :: self
            get_idx = self%idx
        end function get_idx

        elemental subroutine set(self, new_stock)
            class(Good), intent(in out) :: self
            integer, intent(in) :: new_stock
            self%stock = new_stock
        end subroutine set

        elemental subroutine increment(self)
            class(Good), intent(in out) :: self
            self%stock = self%stock + 1
        end subroutine increment

        elemental subroutine decrement(self, lerr, msg)
            class(Good), intent(in out) :: self
            logical, optional, intent(out) :: lerr
            character(kind = CK, len = 200), optional, intent(out) :: msg
            if (self%stock < 1) then
                if (present(lerr) .and. present(msg)) then
                    lerr = .true.
                    msg = CK_"Kein " // self%get_name() // CK_" vorhanden"
                end if
            else
                if (present(lerr) .and. present(msg)) then
                    lerr = .false.
                    msg = self%get_name() // CK_"abgegeben"
                end if
                self%stock = self%stock - 1
            end if
        end subroutine decrement

        elemental logical function lisenough(self, amount)
            class(Good), intent(in) :: self
            integer, intent(in) :: amount
            if (self%stock < amount) then
                lisenough = .false.
            else
                lisenough = .true.
            end if
        end function lisenough

end module mod_good
