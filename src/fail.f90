module mod_fail

    use mod_encoding, only: CK, DEFAULT

    implicit none

contains
    pure subroutine fail(line, filename, msg)
        integer, intent(in) :: line
        character(kind = CK, len = *), intent(in), optional :: msg
        character(kind = DEFAULT, len = *), intent(in) :: filename
        character(kind = DEFAULT, len = 200) :: err_msg
        if (present(msg)) then
            write(err_msg, '(A, I4, A1, A)') msg // new_line(CK_'A') // CK_"Error occured in ", line, CK_":", filename
        else
            write(err_msg, '(A, I4, A1, A)') CK_"Error occured in ", line, CK_":", filename
        end if
        error stop err_msg
    end subroutine fail
end module mod_fail
