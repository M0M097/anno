program anno

    use mod_encoding, only: CK, DEFAULT, UE, OE, NL
    use mod_dice, only: my_dice
    use mod_island, only: &
        take_island, &
        loose_island
    use mod_player, only: Player
    use mod_map, only: world
    use mod_public_buildings, only: &
        PublicBuilding, &
        set_bank_public_buildings, &
        choose_public_building
    use mod_fail, only: fail
    use mod_info, only: send_global_info
    use mod_io, only: &
        STDOUT, STDERR, &
        DELIMITER, &
        idecision, &
        flatten_str, &
        init_io
    use mod_const, only: &
        MAX_NUM_HOUSES, &
        ACTION_MENU, &
        SAIL_MENU, &
        NUM_GOODS, &
        MAX_PLAYER

    implicit none
    type(Player), allocatable :: players(:)
    type(PublicBuilding) :: public_building
    integer :: roll
    integer :: iplayer, jplayer, iship, isail ! Loop indices
    integer :: num_players
    integer :: decision
    integer :: good_idx
    integer :: house_idx
    logical :: lerror
    logical :: lchoose
    logical :: ldid_sail
    logical :: lloose_island
    logical :: lnew_public_building
    integer :: iisland_type
    integer :: iisland_wpos, iisland_hpos
    integer :: ios
    character(kind = CK, len = 200) :: msg
    character(kind = DEFAULT, len = 20) :: player_name

    call init_io

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!! Number of Players !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    num_players = command_argument_count()
    if (num_players > MAX_PLAYER) then
        ! call usage()
        call fail(__LINE__, __FILE__, CK_"maximum number of players is 4")
    else if (num_players == 1) then
        ! call usage()
        write(STDERR, '(A)') CK_"Playing with 1 player is only implemented for testing"
        flush(STDERR)
    end if

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Initalize !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    allocate(players, source = Player([(iplayer, iplayer = 0, num_players - 1)], num_players))
    set_player_name: do iplayer = 1, num_players
        call get_command_argument(number = iplayer, value = player_name, status = ios)
        if (ios /= 0) call fail(__LINE__, __FILE__, CK_"Could not read names from command line")
        call players(iplayer)%set_name(trim(adjustl(player_name)))
    end do set_player_name
    call set_bank_public_buildings(num_players)
    call world%init(num_players)
    ! Greetign message
    call players%send_msg(char(27, kind = CK) // CK_"c")
    call players%send_msg(DELIMITER)
    call players%send_msg(CK_"Ein neues Anno Spiel gestartet" // NL // DELIMITER)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Main Loop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    turns: do
        loop_players: do iplayer = 1, num_players
            associate(player => players(iplayer))
            call player%io_pipe%connect
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!  Phase 1  !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            call send_global_info(num_players, players)
            call players%send_msg(player%get_name() // CK_" ist am Zug.")
            roll = my_dice%roll(msg)
            call players%send_msg(msg)
            if (roll == 6) then
                roll = my_dice%roll(msg)
                do jplayer = 1, num_players
                    if (roll < 3) then ! pirate attack
                        call players(jplayer)%pirate_attack(msg, lloose_island)
                        if (lloose_island) then
                            call loose_island(players(jplayer), iisland_type, msg)
                            call world%return_island(iisland_type, num_players)
                        end if
                    else if (roll < 5) then ! fire
                        call players(jplayer)%fire(msg)
                    else ! golden_age
                        call players%send_msg(CK_"Goldene Zeiten" // NL // &
                            players(jplayer)%get_name() // CK_" darf aussuchen" &
                        )
                        call players(jplayer)%choose(msg)
                        if (players(jplayer)%lhas_church()) then
                            call players%send_msg(trim(adjustl(msg)) // &
                                CK_" Aufgrund einer Kirche darf nochmal ausgesucht werden." &
                            )
                            call players(jplayer)%choose(msg)
                        end if
                    end if
                    call players%send_msg(msg)
                end do
                ! cycle ! Uncomment, if you want to play with alternative rule
            else
                do jplayer = 1, num_players
                    call players(jplayer)%get_goods(roll, msg, lchoose)
                    if (lchoose) then ! choose a good
                        call players%send_msg(msg)
                        call players(jplayer)%choose(msg)
                    end if
                    call players%send_msg(msg)
                end do
            end if
            call player%send_msg(NL // player%get_name() // CK_", dr" // UE // CK_"cke Enter um fortzufahren")
            read(unit = player%io_pipe%get_input_unit(), fmt = '(A)')  msg ! msg is just a dummy here
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!  Phase 2  !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            call send_global_info(num_players, players)
            call players%send_msg(player%get_name() // CK_" ist am Zug.")
            loop_actions: do
                write(STDOUT, '(A, A)') player%get_name(), CK_", was m" // OE // CK_"chest du tun?"
                write(STDOUT, '(A)') ACTION_MENU
                decision = idecision(6)
                lerror = .false.

                select case (decision)

                case (1) ! Buy good
                    call player%send_msg( &
                        CK_"Welchen Rohstoff m" // OE // CK_"chtest du kaufen?" // NL // &
                        player%good_menu() &
                    )
                    good_idx = idecision(NUM_GOODS)
                    call player%buy(good_idx, err = lerror, msg = msg)

                case (2) ! Upgrade house
                    write(STDOUT, '(A)') CK_"Welches Haus m" // OE // CK_"chtest du updaten"
                    call player%print_village
                    house_idx = idecision(MAX_NUM_HOUSES)
                    call player%upgrade_house(house_idx, lerror, msg, lnew_public_building)
                    if (lnew_public_building) then
                        call players%send_msg(msg)
                        call choose_public_building(player%get_name(), lerror, msg, public_building)
                        call player%new_public_building(public_building)
                    end if

                case (3) ! Sell good
                    call player%send_msg( &
                        CK_"Welchen Rohstoff m" // OE // CK_"chtest du verkaufen?" // NL // &
                        player%good_menu() &
                    )
                    good_idx = idecision(NUM_GOODS)
                    call player%sell(good_idx, lerr = lerror, msg = msg)

                case (4) ! Build ship
                    call player%build_ship(lerror, msg)

                case (5) ! Display infos
                    call send_global_info(num_players, players, receiver = player)
                    cycle
                    ! call player%print

                case (6) ! Next phase
                    exit

                case default
                    call fail(__LINE__, __FILE__, CK_"Unhandeld error")

                end select

                if (lerror) then
                    call player%send_msg(msg)
                else
                    call players%send_msg(msg)
                end if

                if (player%get_points() >= 3) then
                    call players%send_msg(player%winning_msg())
                    stop
                end if
            end do loop_actions
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!  Phase 3  !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            loop_ships: do iship = 1, size(player%ships)
                associate(ship => player%ships(iship))
                if (.not. ship%lis_build()) cycle
                isail = 1
                loop_move: do while (isail <= player%get_velocity())
                    write(STDOUT, '(A, I1, A)') CK_"Noch ", player%get_velocity() - isail + 1, &
                        CK_" Z" // UE // CK_"ge m" // OE // CK_"glich."
                    write(STDOUT, '(A, I1, A)') CK_"Wohin m" // OE // CK_"chtest du mit Schiff ", iship, CK_" segeln?"
                    flush(STDOUT)
                    call players%send_msg(flatten_str(world%csnapshot(ship%get_position(), iship)))
                    write(STDOUT, '(A)') SAIL_MENU
                    decision = idecision(4)
                    call ship%sail(decision, ldid_sail, iisland_type, msg, iisland_wpos, iisland_hpos)
                    call player%send_msg(msg)
                    if (ldid_sail) isail = isail + 1
                    if (iisland_type > 0) then
                        if (idecision(2) == 1) then
                            call take_island(iisland_type, player, ship, lerror, msg)
                            call players%send_msg(msg)
                            if (.not. lerror) call world%island2bar(iisland_wpos, iisland_hpos)
                            if (player%get_points() >= 3) then
                                call players%send_msg(player%winning_msg())
                                stop
                            end if
                            if (.not. ship%lis_build()) exit
                        end if
                    end if
                end do loop_move
                end associate
            end do loop_ships

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!  Phase 4  !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            call player%end_turn
            end associate
        end do loop_players
    end do turns
end program
