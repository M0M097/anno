module mod_money

    use mod_const, only: MAX_MONEY
    use mod_encoding, only: CK

    implicit none
    private
    public :: Money

    integer, parameter :: INITIAL_MONEY = 7

    type :: Money
        private
        integer :: amount = INITIAL_MONEY
    contains
        private

        procedure, pass :: set
        procedure, pass :: get

        procedure, pass, public :: subtract
        procedure, pass, public :: add
        procedure, pass, public :: lisenough
        procedure, pass, public :: str_for_print
    end type

    contains

        elemental character(kind = CK, len = 7) function str_for_print(self)
            class(Money), intent(in) :: self
            write(str_for_print, '(I2, 1x, A)') self%amount, CK_"Gold"
        end function str_for_print

        elemental integer function get(self)
            class(Money), intent(in) :: self
            get = self%amount
        end function get

        elemental subroutine set(self, amount)
            class(Money), intent(in out) :: self
            integer, intent(in) :: amount
            self%amount = amount
            if (self%amount > MAX_MONEY) self%amount = MAX_MONEY
        end subroutine

        elemental subroutine subtract(self, amount)
            class(Money), intent(in out) :: self
            integer, intent(in) :: amount
            call self%set(self%amount - amount)
        end subroutine

        elemental subroutine add(self, amount)
            class(Money), intent(in out) :: self
            integer, intent(in) :: amount
            call self%set(self%amount + amount)
        end subroutine

        elemental logical function lisenough(self, amount)
            class(Money), intent(in) :: self
            integer, intent(in) :: amount
            lisenough = self%amount >= amount
        end function

end module mod_money
