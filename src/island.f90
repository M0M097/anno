module mod_island

    use mod_ship, only: Ship
    use mod_player, only: Player
    use mod_const, only: MAX_HARBOUR, INDI, TRS1, TRS2, NISLANDS_PER_PLAYER
    use mod_io, only: idecision, STDOUT
    use mod_encoding, only: CK, AE, OE

    implicit none
    private

    public :: take_island
    public :: loose_island

    contains

        impure recursive subroutine take_island(iisland_type, sailing_player, discovering_ship, lerror, msg)
            integer, intent(in) :: iisland_type
            type(Player), intent(in out) :: sailing_player
            type(Ship), intent(in out) :: discovering_ship
            logical, intent(out) :: lerror
            integer :: decision
            character(kind = CK, len = 200), intent(out) :: msg

            lerror = .true.

            write(msg, '(A, A)') sailing_player%get_name(), CK_" hat die Insel nicht genommen."

            select case (iisland_type)

            case (INDI) ! Indian island
                if (sailing_player%lmax_trade_partners_reached()) then
                    write(STDOUT, '(A)') CK_"Bereits maximale Anzahl an Handelspartnern ereicht"
                else
                    write(msg, '(A)') sailing_player%get_name() // CK_" hat eine Eingeboreneninsel entdeckt!"
                    call sailing_player%increment_trade_partners
                    call discovering_ship%destroy
                    lerror = .false.
                end if

            case (TRS1) ! Treasure: upgrade house
                write(STDOUT, '(A)') CK_"Welches Haus m" // OE // CK_"chtest du aufwerten?"
                call sailing_player%print_village
                decision = idecision(sailing_player%nhouses())
                call sailing_player%upgrade_house_for_free(decision, lerror, msg)
                if (lerror) then
                    call sailing_player%send_msg( &
                        msg // new_line(CK_'A') // &
                        CK_"1) Abbrechen" // new_line(CK_'A') // &
                        CK_"2) Anderes Haus w" // AE // CK_"hlen" &
                    )
                    if (idecision(2) == 2) call take_island(iisland_type, sailing_player, discovering_ship, lerror, msg)
                else
                    call discovering_ship%destroy
                    lerror = .false.
                end if

            case (TRS2) ! Treasure: 12 Gold
                call sailing_player%gold%add(12)
                write(msg, '(A, A)') sailing_player%get_name(), CK_" hat einen Goldschatz entdeckt (+12 Gold)"
                call discovering_ship%destroy
                lerror = .false.

            ! Production island
            case default
                write(STDOUT, '(A)') CK_"Wo m" // OE // CK_"chtest du sie anlegen?"
                write(STDOUT, '(I1, A)') MAX_HARBOUR + 1, CK_") Abbrechen"
                call sailing_player%print_islands
                decision = idecision(MAX_HARBOUR + 1)

                ! Harbour already occupied
                if (sailing_player%lharbour_occupied(decision)) then
                    write(STDOUT, '(A)') CK_"Hafen bereits belegt"
                    call take_island(iisland_type, sailing_player, discovering_ship, lerror, msg)
                ! If no abbort, then take island
                else if (decision /= MAX_HARBOUR + 1) then
                    call sailing_player%connect_harbour(decision, iisland_type, msg)
                    call discovering_ship%destroy
                    lerror = .false.
                end if
            end select
        end subroutine take_island

        impure recursive subroutine loose_island(plyr, iisland_type, msg)
            type(Player), intent(in out) :: plyr
            integer, intent(out)  :: iisland_type
            character(kind = CK, len = 200), intent(in out) :: msg
            logical :: lerror

            call plyr%send_msg(msg)
            call plyr%print_islands
            call plyr%print_trade_partners_menu
            call plyr%loose_island( &
                idecision(NISLANDS_PER_PLAYER, plyr%io_pipe),  &
                iisland_type,  &
                lerror &
            )

            if (lerror) then
                write(msg, '(A)') CK_"Du musst einen besetzen Hafen ausw" // AE // CK_"hlen"
                call loose_island(plyr, iisland_type, msg)
            end if

            write(msg, '(A, A)') plyr%get_name(), CK_" hat eine Insel abgegeben"
        end subroutine loose_island

end module mod_island
