module mod_dice

    use mod_encoding, only: CK, UE
    use mod_well512a, only: random_uniform

    implicit none

    private

    ! Paramters
    integer, parameter :: ONE = 1
    integer, parameter :: SIX = 6

    type dice
    contains
        private
        procedure, pass :: rand_int
        procedure, pass, public :: roll
        procedure, pass, public :: pirate_roll
    end type

    type(dice), public :: my_dice

contains

    !! Random integer between 1 and 6
    impure integer function roll(self, msg)
        class(dice), intent(in out) :: self
        character(kind = CK, len = 200), intent(out), optional :: msg
        roll = self%rand_int(ONE, SIX)
        if (present(msg)) write(msg, '(A, I1)') CK_"Das Ergebnis des W" // UE // CK_"rfelwurfes ist ", roll
    end function roll

    !! Random integer between first and last.
    impure integer function rand_int(self, first, last)
        class(dice), intent(in out) :: self
        integer, intent(in) :: first, last
        rand_int = first - 1 + ceiling(random_uniform() * (last - first + 1))
    end function rand_int

    !! Like roll, but results are 1, 2, 2, 3, 3, 4
    impure integer function pirate_roll(self) result(rand_int)
        class(dice), intent(in out) :: self
        rand_int = self%roll()
        if (rand_int == 5) then
            rand_int = 2
        else if (rand_int == 6) then
            rand_int = 3
        end if
    end function pirate_roll

end module mod_dice
