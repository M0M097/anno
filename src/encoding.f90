module mod_encoding

    implicit none

    integer, parameter :: UCS4    = selected_char_kind('ISO_10646')
    integer, parameter :: ASCII   = selected_char_kind('ASCII')
    integer, parameter :: DEFAULT = selected_char_kind('DEFAULT')

#ifdef USE_ASCII
    integer, parameter, public :: CK = ASCII
    character(kind = CK, len = 2), parameter :: UE = "ü"
    character(kind = CK, len = 2), parameter :: OE = "ö"
    character(kind = CK, len = 2), parameter :: AE = "ä"
    character(kind = CK, len = 2), parameter :: SZ = "ß"

    character(kind = CK), parameter :: VBAR = "-"
    character(kind = CK), parameter :: HBAR = "|"
    character(kind = CK), parameter :: U253C = "|"
    character(kind = CK), parameter :: U2534 = "-"
#else
    integer, parameter, public :: CK = UCS4
    character(kind = CK), parameter :: UE = char(int(Z'00FC'), kind = CK)
    character(kind = CK), parameter :: OE = char(int(Z'00F6'), kind = CK)
    character(kind = CK), parameter :: AE = char(int(Z'00E4'), kind = CK)
    character(kind = CK), parameter :: SZ = char(int(Z'00DF'), kind = CK)

    character(kind = CK), parameter :: HBAR = char(int(Z'2502'), kind = CK)
    character(kind = CK), parameter :: VBAR = char(int(Z'2500'), kind = CK)
    character(kind = CK), parameter :: U253C = char(int(Z'253C'), kind = CK)
    character(kind = CK), parameter :: U2534 = char(int(Z'2534'), kind = CK)
#endif

    character(kind = CK), parameter :: NL = new_line(CK_'A')

    ! Colors
    character(kind = CK, len = *), parameter :: BLK = achar(27, kind = CK) // CK_'[30m'
    character(kind = CK, len = *), parameter :: RED = achar(27, kind = CK) // CK_'[31m'
    character(kind = CK, len = *), parameter :: GRN = achar(27, kind = CK) // CK_'[32m'
    character(kind = CK, len = *), parameter :: YEL = achar(27, kind = CK) // CK_'[33m'
    character(kind = CK, len = *), parameter :: BLU = achar(27, kind = CK) // CK_'[34m'
    character(kind = CK, len = *), parameter :: MAG = achar(27, kind = CK) // CK_'[35m'
    character(kind = CK, len = *), parameter :: CYN = achar(27, kind = CK) // CK_'[36m'
    character(kind = CK, len = *), parameter :: WHT = achar(27, kind = CK) // CK_'[37m'
    character(kind = CK, len = *), parameter :: RESET = achar(27, kind = CK) // CK_'[0m'

    integer, parameter :: CLEN = len(BLK)
    integer, parameter :: RESETLEN = len(RESET)

end module mod_encoding
