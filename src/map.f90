module mod_map
    use mod_const, only: SEA, BARR, DELIM, NFIELD, &
        INDI, SPIC, TBCO, TRS1, TRS2, TOOL, WOOD, WOOL, STON
    use mod_fail, only: fail
    use mod_encoding, only: CK, DEFAULT
    use mod_io, only: center
    use mod_well512a, only: shuffle

    implicit none

    private

    integer, parameter :: WIDTH = 12, HEIGHT = 5
    integer, parameter :: TWO = 2
    integer, parameter :: THRE = 3
    integer, parameter :: FOUR = 4

    integer, parameter, public :: NLINES = HEIGHT + 3

    integer, parameter :: INITIAL_MAP(WIDTH, HEIGHT) = reshape([ &
        &  SEA,  SEA, SEA,  SEA,  SEA, FOUR,  TWO, FOUR, FOUR, SEA,  TWO,  SEA, &
        &  SEA,  SEA, SEA,  TWO, THRE, FOUR,  SEA,  SEA,  SEA, SEA, THRE,  TWO, &
        &  SEA,  SEA, SEA,  SEA,  SEA,  SEA,  SEA,  TWO,  TWO, SEA,  SEA,  SEA, &
        &  SEA,  SEA, SEA,  TWO,  SEA, FOUR,  SEA, THRE, FOUR, SEA, THRE,  TWO, &
        & FOUR,  TWO, SEA, THRE, THRE,  TWO,  SEA,  TWO,  TWO, SEA,  TWO,  SEA  &
    ], [WIDTH, HEIGHT])

    integer :: stock_islands(16, 2:4)
    data stock_islands(1:16, 2) / 6 * INDI, 4 * SPIC, 3 * TBCO, TRS1, TRS2, WOOL /, &
        stock_islands(1:8, 3) / 2 * INDI, SPIC, TBCO, TRS1, TRS2, TOOL, STON /, &
        stock_islands(1:8, 4) / 3 * INDI, SPIC, TBCO, TRS1, TRS2, WOOD /

    type :: Map
        private
        integer :: map(WIDTH, HEIGHT) = INITIAL_MAP
    contains
        private
        procedure, pass :: symbol
        procedure, pass :: csnapshot_without_ship
        procedure, pass :: csnapshot_with_ship
        procedure, pass :: csnapshot_with_ships

        generic, public :: csnapshot => &
            csnapshot_with_ship, &
            csnapshot_without_ship

        procedure, pass, public :: csnapshot_str
        procedure, pass, public :: init
        procedure, pass, public :: lis_sea
        procedure, pass, public :: lis_barr
        procedure, pass, public :: lis_island
        procedure, pass, public :: iisland_type
        procedure, pass, public :: island2bar
        procedure, pass, public :: return_island
    end type Map

    type(Map), public :: world

contains

    impure elemental subroutine init(self, nplayer)
        class(Map), intent(out) :: self
        integer, intent(in) :: nplayer
        integer :: next_card(2:4)
        integer :: i, j

        next_card = 0

        call shuffle(16, stock_islands(:, 2))
        call shuffle( 8, stock_islands(:, 3))
        call shuffle( 8, stock_islands(:, 4))

        do j = 1, HEIGHT
            do i = 1, WIDTH
                if (self%map(i, j) == SEA .or. self%map(i, j) == BARR) then
                    cycle
                else if (self%map(i, j) <= nplayer) then
                    next_card(self%map(i, j)) = next_card(self%map(i, j)) + 1
                    self%map(i, j) = stock_islands(next_card(self%map(i, j)), self%map(i, j))
                else
                    self%map(i, j) = BARR
                end if
            end do
        end do
    end subroutine init

    logical elemental function loutside_map(w_coord, h_coord)
        integer, intent(in) :: h_coord, w_coord
        loutside_map = (w_coord < 1) .or. (w_coord > WIDTH) .or. &
            &          (h_coord < 1) .or. (h_coord > HEIGHT)
    end function loutside_map

    logical elemental function lis_sea(self, w_coord, h_coord)
        class(Map), intent(in) :: self
        integer, intent(in) :: h_coord, w_coord
        lis_sea = .false.
        if (loutside_map(w_coord, h_coord)) return
        if (self%map(w_coord, h_coord) == SEA) lis_sea = .true.
    end function lis_sea

    logical elemental function lis_barr(self, w_coord, h_coord)
        class(Map), intent(in) :: self
        integer, intent(in) :: h_coord, w_coord
        lis_barr = .false.
        if (loutside_map(w_coord, h_coord)) return
        if (self%map(w_coord, h_coord) == BARR) lis_barr = .true.
    end function lis_barr

    logical elemental function lis_island(self, w_coord, h_coord)
        class(Map), intent(in) :: self
        integer, intent(in) :: h_coord, w_coord
        lis_island = .false.
        if (loutside_map(w_coord, h_coord)) return
        if (self%map(w_coord, h_coord) > 0) lis_island = .true.
    end function lis_island

    integer elemental function iisland_type(self, w_coord, h_coord)
        class(Map), intent(in) :: self
        integer, intent(in) :: h_coord, w_coord
        if (.not. self%lis_island(w_coord, h_coord)) then
            iisland_type = -2
        else
            iisland_type = self%map(w_coord, h_coord)
        end if
    end function iisland_type

    elemental subroutine island2bar(self, w_coord, h_coord)
        class(Map), intent(in out) :: self
        integer, intent(in) :: w_coord, h_coord
        if (.not. self%lis_island(w_coord, h_coord)) call fail (__LINE__, __FILE__, CK_"is not an island")
        self%map(w_coord, h_coord) = BARR
    end subroutine island2bar

    pure function csnapshot_with_ship(self, ship_pos, iship) result(csnapshot)
        class(Map), intent(in) :: self
        integer, intent(in) :: ship_pos(2)
        integer, intent(in) :: iship
        character(kind = CK) :: csnapshot(WIDTH, HEIGHT)
        csnapshot = self%symbol(self%map)
        write(csnapshot(ship_pos(1), ship_pos(2)), '(I1)') iship
    end function csnapshot_with_ship

    ! This is not a nice solution
    pure subroutine csnapshot_with_ships(self, ship_pos, cship_name, csnapshot)
        class(Map), intent(in) :: self
        character(kind = CK), intent(in) :: cship_name(:)
        integer, intent(in) :: ship_pos(2, size(cship_name))
        character(kind = CK), intent(out) :: csnapshot(WIDTH, HEIGHT)
        integer :: i
        integer :: ship_positions(WIDTH, HEIGHT)

        ship_positions = 0
        csnapshot = self%symbol(self%map)

        do i = 1, size(ship_pos, 2)
            if (any(ship_pos(:, i) == 0)) cycle
            ship_positions(ship_pos(1, i), ship_pos(2, i)) = ship_positions(ship_pos(1, i), ship_pos(2, i)) + 1
        end do

        do i = 1, size(ship_pos, 2)
            if (any(ship_pos(:, i) == 0)) cycle
            select case (ship_positions(ship_pos(1, i), ship_pos(2, i)))
            case (1)
                write(csnapshot(ship_pos(1, i), ship_pos(2, i)), '(A1)') cship_name(i)
            case default
                write(csnapshot(ship_pos(1, i), ship_pos(2, i)), '(I1)') &
                    ship_positions(ship_pos(1, i), ship_pos(2, i))
            end select
        end do
    end subroutine csnapshot_with_ships

    ! This is not a nice solution
    pure function csnapshot_str(self, ship_pos, cship_name) result(csnapshot)
        class(Map), intent(in) :: self
        character(kind = CK), intent(in) :: cship_name(:)
        integer, intent(in) :: ship_pos(2, size(cship_name))
        character(kind = CK, len = NFIELD) :: csnapshot(HEIGHT + 3)
        integer :: i, j
        character(kind = CK) :: map_cs(WIDTH, HEIGHT)
        character(kind = DEFAULT, len=6) :: ffmt
        character(kind = CK, len = WIDTH) :: map_line

        csnapshot(1) = center(CK_"Seekarte")
        csnapshot(2) = DELIM

        call self%csnapshot_with_ships(ship_pos, cship_name, map_cs)
        write(ffmt, '(A1, I2, A3)') '(', WIDTH, 'A1)'
        j = 1
        do i = 3, HEIGHT + 2
            write(map_line, ffmt) map_cs(:, j)
            csnapshot(i) = center(map_line)
            j = j + 1
        end do
        csnapshot(HEIGHT + 3) = DELIM
    end function csnapshot_str

    pure function csnapshot_without_ship(self) result(csnapshot)
        class(Map), intent(in) :: self
        character(kind = CK) :: csnapshot(WIDTH, HEIGHT)
        csnapshot = self%symbol(self%map)
    end function csnapshot_without_ship

    character(kind = CK) elemental function symbol(self, id)
        class(Map), intent(in) :: self
        integer, intent(in) :: id
        select case (id)
        case (BARR)
            symbol = CK_"x"
        case (SEA)
            symbol = CK_"~"
        case default
            symbol = CK_"I"
        end select
    end function symbol

    ! Put island back to the place furthest away
    recursive pure subroutine return_island(self, iisland_type, nplayer, input_dist)
        class(Map), intent(in out) :: self
        integer, intent(in) :: iisland_type
        integer, intent(in) :: nplayer
        integer, intent(in), optional :: input_dist
        integer :: w, h, dist

        if (present(input_dist)) then
            dist = input_dist
        else
            dist = WIDTH + HEIGHT
        end if

        if (dist == 0) call fail(__LINE__, __FILE__)

        do h = 1, HEIGHT
            w = dist - h
            if (w <= WIDTH .and. INITIAL_MAP(w, h) <= nplayer .and. self%map(w, h) == BARR) then
                self%map(w, h) = iisland_type
                return
            end if
        end do
        call self%return_island(iisland_type, nplayer, dist - 1)
    end subroutine return_island

end module mod_map
