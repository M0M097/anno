module mod_c

    use, intrinsic :: iso_c_binding, only: c_int

    implicit none

    interface

        subroutine sleep(seconds) bind(c)
            use, intrinsic :: iso_c_binding, only: c_int
            integer(kind = c_int), value, intent(in) :: seconds
        end subroutine

    end interface

end module mod_c

