module mod_io

    use, intrinsic :: iso_fortran_env, only: &
        INPUT_UNIT, &
        output_unit, &
        STDERR => error_unit
    use, intrinsic :: iso_c_binding, only: c_int
    use mod_c, only: sleep
    use mod_fail, only: fail
    use mod_encoding, only: CK, DEFAULT, VBAR, NL
    use mod_const, only: NFIELD

    implicit none

    private

    ! Might be changed to a named pipe
    integer, public, protected :: stdin = INPUT_UNIT
    integer, public, protected :: stdout = OUTPUT_UNIT

    public :: STDERR
    public :: idecision
    public :: flatten_str
    public :: flatten_str_1d
    public :: IoPipe
    public :: write_all
    public :: init_io
    public :: fmt_str
    public :: fill
    public :: center

    character(kind = CK, len = *), parameter, public :: DELIMITER  = repeat(VBAR, 80) // NL

    type IoPipe
        private
        integer :: i_unit
        integer, public :: o_unit
        character(kind = DEFAULT, len =:), allocatable :: infile
    contains
        procedure :: discard
        procedure :: connect
        procedure :: get_input_unit
        procedure :: get_output_unit
    end type IoPipe

    interface IoPipe
        procedure :: constructor
    end interface IoPipe

contains

    type(IoPipe) function constructor(username) result(self)
        character(kind = DEFAULT, len = *), intent(in) :: username
        integer :: ios_in, ios_out
        character(kind = DEFAULT, len = *), parameter :: PREFIX = DEFAULT_"/home/"
        character(kind = DEFAULT, len = *), parameter :: INPUT_SUFFIX = DEFAULT_"/.anno/input"
        character(kind = DEFAULT, len = *), parameter :: OUTPUT_SUFFIX = DEFAULT_"/.anno/output"
        character(kind = DEFAULT, len = :), allocatable :: outfile

        self%infile = PREFIX // username // INPUT_SUFFIX
        outfile = PREFIX // username // OUTPUT_SUFFIX
100     open(newunit = self%i_unit, file = self%infile, iostat = ios_in, encoding = 'utf-8')
        open(newunit = self%o_unit, file = outfile, iostat = ios_out, encoding = 'utf-8')

        if (ios_in /= 0 .or. ios_out /= 0) then
            write(STDOUT, '(A, A)') CK_"Waiting for ", username
            call sleep(5_c_int)
            goto 100
        end if
    end function constructor

    integer elemental function get_input_unit(self)
        class(IoPipe), intent(in) :: self
        get_input_unit = self%i_unit
    end function get_input_unit

    integer elemental function get_output_unit(self)
        class(IoPipe), intent(in) :: self
        get_output_unit = self%o_unit
    end function get_output_unit

    ! Position read head at the end of a named pipe
    impure subroutine discard(self)
        class(IoPipe), intent(in out) :: self
        close(self%i_unit)
        open(newunit = self%i_unit, file = self%infile, encoding = 'utf-8')
    end subroutine discard

    impure subroutine connect(self)
        class(IoPipe), intent(in out) :: self
        call self%discard
        stdin = self%i_unit
        stdout = self%o_unit
    end subroutine connect

    integer impure function idecision(nmax, io_pipe)
        integer, intent(in) :: nmax
        type(IoPipe), intent(in out), optional :: io_pipe
        integer :: ii_unit, io_unit
        integer :: ioflag

        if (present(io_pipe)) then
            call io_pipe%discard
            ii_unit = io_pipe%get_input_unit()
            io_unit = io_pipe%get_output_unit()
        else
            ii_unit = stdin
            io_unit = stdout
        end if

100     flush(io_unit)
        read(unit = ii_unit, fmt = '(I1)', iostat = ioflag) idecision
        if (ioflag /= 0) then
            write(unit = io_unit, fmt = '(A)') CK_"Input konnte nicht korrekt eingelesen werden, bitte wiederholen: "
            goto 100
        else if  (idecision < 1 .or. idecision > nmax) then
            write(unit = io_unit, fmt = '(A, I1, A)') CK_"Wahl muss zwischen 1 und ", nmax, CK_" liegen"
            goto 100
        end if
    end function idecision

    pure function flatten_str(array)
        character(kind = CK), intent(in) :: array(:, :)
        character(kind = CK, len = size(array, 1) * size(array, 2) + size(array, 2)) :: flatten_str
        character(kind = DEFAULT, len = 6) :: ffmt
        integer :: j, i, ios
        integer :: width

        width = size(array, 1)
        write(ffmt, '(A1, I2, A3)') '(', width, 'A1)'

        do j = 1, size(array, 2)
            write(flatten_str((j - 1) * width + (j - 1) + 1:j * width + j - 1), ffmt, iostat = ios) &
                (array(i, j), i = 1, width)
            if (ios /= 0) call fail(__LINE__, __FILE__, CK_"errror flattening array")
            write(flatten_str(j * width + j:j * width + j), '(A1)', iostat = ios) new_line(CK_'A')
            if (ios /= 0) call fail(__LINE__, __FILE__, CK_"errror flattening array")
        end do
    end function flatten_str

    pure function flatten_str_1d(array)
        character(kind = CK, len = *), intent(in) :: array(:)
        character(kind = CK, len = len(array(1)) * size(array) + size(array)) :: flatten_str_1d
        character(kind = DEFAULT, len = 8) :: ffmt
        integer :: j, ios
        integer :: width

        width = len(array(1))
        if (width < 10) then
            write(ffmt, '(A2, I1, A3)') '(A', width, 'A)'
        else if (width < 100) then
            write(ffmt, '(A2, I2, A3)') '(A', width, 'A)'
        else if (width < 1000) then
            write(ffmt, '(A2, I3, A3)') '(A', width, 'A)'
        else
            call fail(__LINE__, __FILE__)
        end if

        do j = 1, size(array)

            write( &
                flatten_str_1d((j - 1) * width + (j - 1) + 1:j * width + j - 1), &
                fmt = trim(adjustl(ffmt)), &
                iostat = ios &
            ) array(j)
            if (ios /= 0) call fail(__LINE__, __FILE__, CK_"errror flattening array")

            write( &
                flatten_str_1d(j * width + j:j * width + j), &
                fmt = '(A1)',  &
                iostat = ios &
            ) new_line(CK_'A')
            if (ios /= 0) call fail(__LINE__, __FILE__, CK_"errror flattening array")

        end do
    end function flatten_str_1d

    ! To write strings to multiple units
    impure elemental subroutine write_all(unit, str)
        integer, intent(in) :: unit
        character(kind = CK, len = *), intent(in) :: str
        write(unit = unit, fmt = '(A)') str
        flush(unit)
    end subroutine write_all

    impure subroutine init_io()
        open(stdout, encoding ='utf-8')
    end subroutine init_io

    pure function fill(ilen)
        integer, intent(in) :: ilen
        character(kind = CK, len = :), allocatable :: fill

        if (NFIELD -ilen < 0) call fail(__LINE__, __FILE__)
        fill = repeat(' ', NFIELD - ilen)
    end function fill

    character(kind = CK, len = NFIELD) pure function fmt_str(in_str) result(str)
        character(kind = CK, len = *), intent(in) :: in_str
        write(str, '(A)') in_str // fill(len(in_str))
    end function fmt_str

    elemental function center(str)
        character(kind = CK, len = *), intent(in) :: str
        character(kind = CK, len = NFIELD) :: center
        integer :: left, right, space

        space = NFIELD - len(trim(adjustl(str)))

        if (space < 0) call fail(__LINE__, __FILE__)

        left = space / 2

        if (mod(space, 2) == 0) then
            right = left
        else
            right = left + 1
        end if

        center = repeat(CK_' ', left) // str // repeat(CK_' ', right)
    end function center

end module mod_io
