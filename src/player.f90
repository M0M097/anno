module mod_player

    use mod_encoding, only: &
        CK, DEFAULT, &
        NL, UE, AE, SZ, &
        CLEN, RESET, &
        HBAR, U2534
    use mod_good, only: Good
    use mod_money, only: Money
    use mod_const, only: &
        NUM_GOODS, &
        GOOD_NAMES, &
        GOOD_INI_STOCK, &
        GOOD_INDEX, &
        IGEWUERZ, &
        MAX_NUM_HOUSES, &
        HOUSE_INI_LEVEL, &
        INITIAL_BUYING_PRICE, &
        MAX_TRADE_PARTNERS, &
        NPUB_BUILDINGS_PER_PLAYER, &
        PB_OFFSET, &
        NISLANDS_PER_PLAYER, &
        COLORS, &
        NFIELD, INFO_STR_LEN, DELIM, &
        NINVENTORY_LINES, &
        INDI
    use mod_house, only: House
    use mod_io, only: &
        idecision, &
        IoPipe, &
        write_all, &
        flatten_str_1d, &
        fmt_str, &
        fill, &
        center, &
        DELIMITER
    use mod_ship, only: Ship
    use mod_public_buildings, only: PublicBuilding

    implicit none

    private

    public :: Player

    type :: Player

        private

        type(Money),  public :: gold
        type(Ship),   public :: ships(2)
        type(IoPipe), public :: io_pipe

        type(Good) :: goods(NUM_GOODS)
        type(House) :: houses(MAX_NUM_HOUSES)
        type(PublicBuilding) :: public_buildings(NPUB_BUILDINGS_PER_PLAYER) = PublicBuilding()
        character(kind = CK, len=:), allocatable :: name
        character(kind = CK, len= CLEN ) :: color
        integer :: goods_mapping
        integer :: ihas_bought = 0
        integer :: ship_velocity
        integer :: islands(NISLANDS_PER_PLAYER) = 0
        integer :: trade_partners = 0

    contains

        private

        procedure, pass :: lhas_pirate_protection
        procedure, pass :: lhas_fire_protection
        procedure, pass :: loose_house
        procedure, pass :: get_buying_price
        procedure, pass :: get_ship_positions
        procedure, pass :: nislands
        procedure, pass :: npublic_buildings
        procedure, pass :: calc_mapping
        procedure, pass :: inventory_str
        procedure, pass :: print_inventory
        procedure, pass :: village_str
        procedure, pass :: island_str

        procedure, pass, public :: get_goods
        procedure, pass, public :: get_points
        procedure, pass, public :: get_velocity
        procedure, pass, public :: get_first_letter
        procedure, pass, public :: good_menu
        procedure, pass, public :: print_islands
        procedure, pass, public :: print_trade_partners_menu
        procedure, pass, public :: print_village
        procedure, pass, public :: loose_island
        procedure, pass, public :: nhouses
        procedure, pass, public :: buy
        procedure, pass, public :: sell
        procedure, pass, public :: upgrade_house
        procedure, pass, public :: upgrade_house_for_free
        procedure, pass, public :: choose
        procedure, pass, public :: fire
        procedure, pass, public :: pirate_attack
        procedure, pass, public :: end_turn
        procedure, pass, public :: lmax_trade_partners_reached
        procedure, pass, public :: build_ship
        procedure, pass, public :: connect_harbour
        procedure, pass, public :: lharbour_occupied
        procedure, pass, public :: increment_trade_partners
        procedure, pass, public :: decrement_trade_partners
        procedure, pass, public :: get_name
        procedure, pass, public :: set_name
        procedure, pass, public :: new_public_building
        procedure, pass, public :: send_msg
        procedure, pass, public :: info_str
        procedure, pass, public :: lhas_church
        procedure, pass, public :: winning_msg
    end type

    interface Player
        module procedure constructor
    end interface Player

contains

        elemental type(Player) function constructor(offset, nplayer) result(self)
            integer, intent(in) :: offset
            integer, intent(in) :: nplayer
            self%ship_velocity = nplayer
            self%goods_mapping = offset
            self%gold = Money()
            self%houses = House(HOUSE_INI_LEVEL)
            self%ships = Ship([.true., .false.])
            self%color = COLORS(offset + 1)
            self%goods = [Good( &
                name          = GOOD_NAMES(1:), &
                idx           = GOOD_INDEX, &
                initial_stock = GOOD_INI_STOCK &
            )]
        end function constructor

        pure function info_str(self) result(str)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD + 1) :: str(INFO_STR_LEN)
            integer :: ilower, iupper

            iupper = 1
            str(iupper) = center(self%name) // HBAR
            str(iupper + 1) = DELIM
            ilower = iupper + 1 + 1
            iupper = iupper + 1 + NINVENTORY_LINES
            call self%inventory_str(str(ilower:iupper)(1:NFIELD))
            str(ilower:iupper)(NFIELD + 1:NFIELD + 1) = HBAR
            str(iupper + 1) = DELIM
            ilower = iupper + 1 + 1
            iupper = iupper + 1 + MAX_NUM_HOUSES
            call self%village_str(str(ilower:iupper)(1:NFIELD))
            str(ilower:iupper)(NFIELD + 1:NFIELD + 1) = HBAR
            str(iupper + 1) = DELIM
            ilower = iupper + 1 + 1
            iupper = iupper + 1 + 1
            write(str(iupper), '(A12, I1, A)') CK_"Siegpunkte: ", self%get_points(), fill(12 + 1) // HBAR
            str(iupper + 1) = DELIM
            ilower = iupper + 1 + 1
            iupper = iupper + 1 + 1
            write(str(iupper), '(A16, I1, A)') CK_"Handelspartner: ", self%trade_partners, fill(16 + 1) // HBAR
            str(iupper + 1) = DELIM
            ilower = iupper + 1 + 1
            iupper = iupper + 1 + NISLANDS_PER_PLAYER
            call self%island_str(str(ilower:iupper)(1:NFIELD))
            str(ilower:iupper)(NFIELD + 1: NFIELD + 1) = HBAR
            str(iupper + 1) = DELIM
            str(iupper + 1)(NFIELD + 1:NFIELD + 1) = U2534
        end function info_str

        pure subroutine inventory_str(self, str)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD), intent(out) :: str(NINVENTORY_LINES)
            integer :: idx
            do idx = 1, NUM_GOODS / 2
                str(idx) = fmt_str(self%goods(idx)%str() // CK_"  " // self%goods(idx + (NUM_GOODS / 2))%str())
            end  do
            str(NINVENTORY_LINES) = fmt_str(self%gold%str_for_print())
        end subroutine inventory_str

        impure subroutine print_inventory(self)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD) :: str(NUM_GOODS + 1)
            call self%inventory_str(str)
            call self%send_msg(str)
        end subroutine print_inventory

        !! Get goods for a given roll of the dice as long as no 6 was rolled
        elemental subroutine get_goods(self, roll, msg, lchoose)
            class(Player), intent(in out) :: self
            integer, intent(in) :: roll ! Result of the dice roll
            character(kind = CK, len = 200), intent(out) :: msg
            integer :: mapping ! Maps the dice roll to the goods we get
            logical, intent(out) :: lchoose

            mapping = self%calc_mapping(roll)
            lchoose = .false.

            if (mapping == 5) then
                msg = self%get_name() // CK_" darf aussuchen."
                lchoose = .true.
            else
                call self%goods(mapping)%increment !! Get a good
                msg = self%get_name() // CK_" hat ein " // self%goods(mapping)%get_name() // CK_" erhalten."
            end if

            ! Islands
            if (self%islands(roll) /= 0) then
                call self%goods(self%islands(roll))%increment
                msg = trim(adjustl(msg)) // NL // &
                    CK_"Wegen einer angelegten Insel erh" // AE // CK_"lt " // &
                    self%get_name() // CK_" au" // SZ // CK_"erdem ein " // &
                    self%goods(self%islands(roll))%get_name()
            end if
        end subroutine

        integer elemental function calc_mapping(self, roll) result(mapping)
            class(Player), intent(in) :: self
            integer, intent(in) :: roll
            mapping = roll + self%goods_mapping
            if (mapping > 5) mapping = mapping - 5
        end function calc_mapping

        !! Get goods for a given roll of the dice as long as no 6 was rolled
        impure elemental subroutine choose(self, msg)
            class(Player), intent(in out) :: self
            character(kind = CK, len = 200), intent(out) :: msg
            integer :: decision

            call self%send_msg(str = self%good_menu())
            decision = idecision(NUM_GOODS, self%io_pipe)
            call self%goods(decision)%increment
            msg = self%get_name() // CK_" hat " // self%goods(decision)%get_name() // CK_" ausgesucht."
        end subroutine choose

        elemental integer function get_buying_price(self)
            class(Player), intent(in) :: self
            get_buying_price = INITIAL_BUYING_PRICE - self%trade_partners
        end function get_buying_price

        pure subroutine village_str(self, str)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD), intent(out) :: str(MAX_NUM_HOUSES)
            integer :: ihouse
            loop_house_without_public_building: do ihouse = 1, PB_OFFSET
                write(str(ihouse), '(I1, A2, A9, A)') ihouse, CK_": ", &
                    self%houses(ihouse)%get_name(), fill(1 + 2 + 9)
            end do loop_house_without_public_building
            loop_house_with_public_building: do ihouse = 1, MAX_NUM_HOUSES - PB_OFFSET
                write(str(ihouse + PB_OFFSET), '(I1, A2, A9, A9, A)') ihouse + PB_OFFSET, CK_": ", &
                    self%houses(ihouse + PB_OFFSET)%get_name(), &
                    self%public_buildings(ihouse)%get_name(), fill(1 + 2 + 9 + 9)
            end do loop_house_with_public_building
        end subroutine village_str

        impure subroutine print_village(self)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD) :: str(MAX_NUM_HOUSES)
            call self%village_str(str)
            call self%send_msg(str)
        end subroutine print_village

        elemental subroutine upgrade_house(self, house_idx, err, msg, lnew_public_building)
            integer, intent(in) :: house_idx
            class(Player), intent(in out) :: self
            logical, intent(out) :: err
            character(kind = CK, len = 200), intent(out) :: msg
            logical, intent(out) :: lnew_public_building
            if ( &
                self%houses(house_idx)%lis_siedler() .and. &
                count(self%houses%lis_buerger()) + count(self%houses%lis_kaufmann()) >= 3 &
            ) then
                msg = CK_"Bereits drei B" // UE // CK_"rger oder Kaufleute vorhanden.."
                err = .true.
                lnew_public_building = .false.
            else if (self%houses(house_idx)%lisbuild()) then
                call self%houses(house_idx)%upgrade(self%goods, err, msg)
                lnew_public_building = .false.
            else if (count(self%houses%lis_pionier()) + count(self%houses%lis_siedler()) >= 4) then
                msg = CK_"Bereits vier Siedler oder Pioniere vorhanden: &
                    &Aufwertung zum B" // UE // CK_"rger erforderlich"
                err = .true.
                lnew_public_building = .false.
            else
                call self%houses(self%nhouses() + 1)%upgrade(self%goods, err, msg)
                lnew_public_building = self%nhouses() > PB_OFFSET .and. .not. err
            end if
        end subroutine upgrade_house

        elemental subroutine new_public_building(self, public_building)
            class(Player), intent(in out) :: self
            type(PublicBuilding), intent(in) :: public_building
            self%public_buildings(self%npublic_buildings() + 1) = public_building
        end subroutine new_public_building

        elemental subroutine upgrade_house_for_free(self, house_idx, err, msg)
            integer, intent(in) :: house_idx
            class(Player), intent(in out) :: self
            logical, intent(out), optional  :: err
            character(kind = CK, len = 200), intent(out), optional :: msg
            if ( &
                self%houses(house_idx)%lis_siedler() .and. &
                count(self%houses%lis_buerger()) + count(self%houses%lis_kaufmann()) >= 3 &
            ) then
                msg = CK_"Bereits drei B" // UE // CK_"rger oder Kaufleute vorhanden.."
                err = .true.
            else
                call self%houses(house_idx)%upgrade_for_free(err, msg)
            end if
        end subroutine upgrade_house_for_free

        elemental subroutine build_ship(self, lerr, msg)
            class(Player), intent(in out) :: self
            logical, intent(out) :: lerr
            character(kind = CK, len = 200), intent(out) :: msg
            integer :: iship
            do iship = 1, size(self%ships)
                if (.not. self%ships(iship)%lis_build()) then
                    call self%ships(iship)%build(self%goods, lerr, msg)
                    return
                end if
            end do
            lerr = .true.
            write(msg, '(A)') CK_"Du hast bereits zwei Schiffe"
        end subroutine build_ship

        elemental subroutine sell(self, good_idx, lerr, msg)
            class(Player), intent(in out) :: self
            integer, intent(in) :: good_idx
            logical, intent(out) :: lerr
            character(kind = CK, len = 200), intent(out) :: msg
            integer :: ihouse

            associate(good => self%goods(good_idx))

            if (.not. good%lisenough(1)) then
                lerr = .true.
                msg = CK_"Kein " // good%get_name() // CK_" vorhanden"
                return
            end if

            do ihouse = 1, self%nhouses()
                if (self%houses(ihouse)%ldoes_buy(good)) then
                    call self%houses(ihouse)%sell( &
                        good, &
                        self%gold,  &
                        self%public_buildings,  &
                        lerr,  &
                        msg &
                    )
                    return
                end if
            end do

            lerr = .true.
            msg = CK_"Kein Einwohner kann (mehr) ein " // good%get_name() // CK_" kaufen"

            end associate

        end subroutine sell

        elemental subroutine buy(self, good_idx, err, msg)
            integer, intent(in) :: good_idx
            class(Player), intent(in out) :: self
            logical, optional, intent(out) :: err
            character(kind = CK, len = 200), optional, intent(out) :: msg

            if (.not. self%gold%lisenough(self%get_buying_price())) then
                err = .true.
                msg = CK_"Not enough money"
            else if (good_idx < 1 .or. good_idx > 6) then
                err = .true.
                msg = CK_"Not a valid index"
            else if (self%ihas_bought == 2) then
                err = .true.
                msg = CK_"Du kannst nur zwei G" // UE // CK_"ter pro Zug kaufen!"
            else
                err = .false.
                call self%gold%subtract(self%get_buying_price())
                call self%goods(good_idx)%increment
                self%ihas_bought = self%ihas_bought + 1
                msg = self%goods(good_idx)%get_name() // CK_" gekauft"
            end if
        end subroutine buy

        impure elemental subroutine end_turn(self)
            class(Player), intent(in out) :: self
            integer :: decision
            logical :: lerr
            character(kind = CK, len = 200) :: msg
            do while (sum(self%goods%get()) > 5)
                call self%send_msg( str = &
                    CK_"Du hast mehr als 5 Rohstoffe und must welche abgeben:" &
                    // NL // self%good_menu() &
                )
                decision = idecision(NUM_GOODS)
                call self%goods(decision)%decrement(lerr, msg)
                if (lerr) call self%send_msg(msg)
            end do
            self%ihas_bought = 0
            call self%houses%reset
        end subroutine end_turn

        pure function good_menu(self)
            class(Player), intent(in) :: self
            character(kind = CK, len = :), allocatable :: good_menu
            character(kind = CK, len = NFIELD) :: tmp_str(NUM_GOODS)
            integer :: igood
            do igood = 1, NUM_GOODS
                tmp_str(igood) = fmt_str(adjustl( &
                    self%goods(igood)%menu_str() &
                ))
            end do
            good_menu = flatten_str_1d(tmp_str)
        end function good_menu

        elemental logical function lhas_pirate_protection(self)
            class(Player), intent(in) :: self
            lhas_pirate_protection = any(self%public_buildings%lis_schmiede())
        end function lhas_pirate_protection

        elemental logical function lhas_fire_protection(self)
            class(Player), intent(in) :: self
            lhas_fire_protection = any(self%public_buildings%lis_feuerwehr())
        end function lhas_fire_protection

        elemental logical function lhas_church(self)
            class(Player), intent(in) :: self
            lhas_church = any(self%public_buildings%lis_kirche())
        end function lhas_church

        elemental integer function nhouses(self)
            class(Player), intent(in) :: self
            nhouses = count(self%houses%lisbuild())
        end function nhouses

        impure elemental subroutine loose_house(self)
            class(Player), intent(in out) :: self
            integer :: ihouse
            do ihouse = self%nhouses(), 3, -1
                if (self%houses(ihouse)%lisbuild()) then
                    ! Destroy associated public building if present
                    if (self%nhouses() - self%npublic_buildings() == PB_OFFSET) &
                        call self%public_buildings(self%npublic_buildings())%destroy
                    ! Destroy house
                    call self%houses(ihouse)%burns_down
                    return
                end if
            end do
        end subroutine loose_house

        elemental subroutine loose_island(self, iisland_idx, iisland_type, lerr)
            class(Player), intent(in out) :: self
            integer, intent(in) :: iisland_idx
            integer, intent(out)  :: iisland_type
            logical, intent(out) :: lerr
            if (iisland_idx == NISLANDS_PER_PLAYER + 1) then
                call self%decrement_trade_partners
                iisland_type = INDI
                lerr = .false.
            else if (self%islands(iisland_idx) == 0) then
                lerr = .true.
            else
                iisland_type = self%islands(iisland_idx)
                self%islands(iisland_idx) = 0
                lerr = .false.
            end if
        end subroutine loose_island

        pure subroutine island_str(self, str)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD), intent(out) :: str(NISLANDS_PER_PLAYER)
            character(kind = CK, len = :), allocatable :: tmp_str, production_name
            integer :: i, imapping

            do i = 1, NISLANDS_PER_PLAYER
                imapping = self%calc_mapping(i)
                if (imapping == IGEWUERZ) then ! Choose good
                    production_name = CK_"M" // UE // CK_"hle"
                else
                    production_name = trim(adjustl(GOOD_NAMES(imapping)))
                end if
                tmp_str = CK_": " // production_name // &
                    CK_" === " // trim(adjustl(GOOD_NAMES(self%islands(i))))
                write(str(i), '(I1, A)') i, tmp_str // fill(1 + len(tmp_str))
                deallocate(tmp_str, production_name)
            end do
        end subroutine island_str

        impure subroutine print_islands(self)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD) :: str(NISLANDS_PER_PLAYER)
            call self%island_str(str)
            call self%send_msg(str)
        end subroutine print_islands

        impure subroutine print_trade_partners_menu(self)
            class(Player), intent(in) :: self
            character(kind = CK, len = NFIELD) :: str
            write(str, '(I1, A, I1, A1)') &
                NISLANDS_PER_PLAYER + 1, CK_": Handelspartner(", self%trade_partners, CK_")"
            call self%send_msg(str)
        end subroutine print_trade_partners_menu

        elemental integer function nislands(self)
            class(Player), intent(in) :: self
            nislands = count(self%islands >= 1)
        end function nislands

        elemental integer function npublic_buildings(self)
            class(Player), intent(in) :: self
            npublic_buildings = count(self%public_buildings%lis_build())
        end function npublic_buildings

        impure elemental subroutine fire(self, msg)
            class(Player), intent(in out) :: self
            character(kind = CK, len = 200), intent(out) :: msg
            integer :: costs
            costs = self%nhouses() - 2
            if (self%lhas_fire_protection()) then
                write(msg, '(A, A)') self%name, CK_" ist vor der Feuerbrunst gesch" // UE // CK_"tzt"
            else if (self%gold%lisenough(costs)) then
                write(msg, '(A, A, I1, A)') &
                    self%name, CK_" verliert wegen der Feuerbrunst , ", costs, CK_" gold."
                call self%gold%subtract(costs)
            else
                write(msg, '(A, A)') self%name, CK_" hat nicht genug Geld, ein Haus brennt ab"
                call self%loose_house
            end if
        end subroutine fire

        elemental subroutine pirate_attack(self, msg, lloose_island)
            class(Player), intent(in out) :: self
            character(kind = CK, len = 200), intent(out) :: msg
            logical, intent(out) :: lloose_island
            integer :: costs

            lloose_island = .false.
            costs = self%nislands() + self%trade_partners
            if (self%lhas_pirate_protection()) then
                write(msg, '(A, A)') self%name, &
                    CK_" ist vor Piraten" // UE // CK_"berf" // AE // CK_"llen gesch" // UE // CK_"tzt"
            else if (self%gold%lisenough(costs)) then
                write(msg, '(A, A, I1, A)') &
                    self%name, CK_" verliert wegen des Piraten" // UE // CK_"berfalls, ", costs, CK_" gold."
                call self%gold%subtract(costs)
            else
                write(msg, '(A)') CK_"Du hast nicht genug Geld und verlierst wegen des &
                    &Piraten" // UE // CK_"berfalls eine Insel"
                lloose_island = .true.
            end if
        end subroutine pirate_attack

        elemental integer function get_velocity(self)
            class(Player), intent(in) :: self
            if (any(self%public_buildings%lis_werft())) then
                get_velocity = self%ship_velocity * 2
            else
                get_velocity = self%ship_velocity
            end if
        end function get_velocity

        pure function get_ship_positions(self)
            class(Player), intent(in) :: self
            integer :: get_ship_positions(2, 2)
            integer :: iship
            do iship = 1, 2
                get_ship_positions(:, iship) = self%ships(iship)%get_position()
            end do
        end function get_ship_positions

        elemental integer function get_points(self)
            class(Player), intent(in) :: self
            get_points = 0
            if (self%npublic_buildings() == NPUB_BUILDINGS_PER_PLAYER) get_points = get_points + 1
            if (self%nislands() == 4) get_points = get_points + 1
            if (self%lmax_trade_partners_reached()) get_points = get_points + 1
            if (count(self%houses%lis_kaufmann()) == 3) get_points = get_points + 1
            if (self%gold%lisenough(30)) get_points = get_points + 1
        end function get_points

        elemental subroutine connect_harbour(self, harbour_idx, iisland_type, msg)
            class(Player), intent(in out) :: self
            integer, intent(in) :: harbour_idx
            integer, intent(in) :: iisland_type
            character(kind = CK, len = 200), intent(out) :: msg
            self%islands(harbour_idx) = iisland_type
            write(msg, '(4A, I1, A)') self%name, CK_" hat eine ", &
                trim(adjustl(GOOD_NAMES(iisland_type))), CK_" Insel an Postion ", harbour_idx, CK_" angelegt."
        end subroutine connect_harbour

        elemental logical function lharbour_occupied(self, harbour_idx)
            class(Player), intent(in) :: self
            integer, intent(in) :: harbour_idx
            lharbour_occupied = self%islands(harbour_idx) /= 0
        end function lharbour_occupied

        elemental logical function lmax_trade_partners_reached(self)
            class(Player), intent(in) :: self
            lmax_trade_partners_reached = self%trade_partners == MAX_TRADE_PARTNERS
        end function lmax_trade_partners_reached

        elemental subroutine decrement_trade_partners(self)
            class(Player), intent(in out) :: self
            self%trade_partners = self%trade_partners - 1
        end subroutine decrement_trade_partners

        elemental subroutine increment_trade_partners(self)
            class(Player), intent(in out) :: self
            self%trade_partners = self%trade_partners + 1
        end subroutine increment_trade_partners

        pure function get_name(self)
            class(Player), intent(in) :: self
            character(kind = CK, len = :), allocatable :: get_name
            get_name = self%color // trim(adjustl(self%name)) // RESET
        end function get_name

        impure elemental subroutine set_name(self, name)
            class(Player), intent(in out) :: self
            character(kind = DEFAULT, len = *), intent(in) :: name
            self%name = name
            self%io_pipe = IoPipe(name)
        end subroutine set_name

        impure elemental subroutine send_msg(self, str)
            class(Player), intent(in) :: self
            character(kind = CK, len = *), intent(in) :: str
            call write_all(self%io_pipe%o_unit, str)
        end subroutine send_msg

        character(kind = CK) elemental function get_first_letter(self)
            class(Player), intent(in) :: self
            get_first_letter = self%name(1:1)
        end function get_first_letter

        pure function winning_msg(self)
            class(Player), intent(in) :: self
            character(kind = CK, len = :), allocatable :: winning_msg
            winning_msg = DELIMITER // NL // &
                self%get_name() // CK_" hat gewonnen!!!" // NL // &
                DELIMITER
        end function winning_msg

end module mod_player
